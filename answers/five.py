import base64


def decode_hex_message(hex_message):
    bytes_obj = bytes.fromhex(hex_message)
    text = bytes_obj.decode("utf-8", errors="ignore")
    return text


def decode_base64(encoded_message):
    decoded_message = base64.b64decode(encoded_message).decode("utf-8")
    return decoded_message
