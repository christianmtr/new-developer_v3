>___Create a file telling us when you last used RTFM and LMGTFY,
the OS you use and the languages you master___

There are many situations where we had to use RTFM and LMGTF with a team member or for ourselves. If there are any questions, I always recommend asking questions in the team's channels and doing a quick search in the documentation and your favorite search engine, we are constantly learning, so asking or looking for something we don't know should always be an option.
I'm an expert Python/Django developer, with more than 7 years building backend software like Restful API. As a senior developer, I can guide and share knowledge with the team. I also learned that opening the documentation or searching for something is a good practice when I can't remember a built-in function or method I haven't used in a while.
