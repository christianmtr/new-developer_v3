## 3


A developer's portfolio is important to us. We ask you to upload 1 or 2 
projects of your authorship.

>___If you do not want to share the code, you can just paste some of it.___


I have not code of main projects of my previous jobs, but I have some small personal projects and microservices that we used as a complement for the product.

1. This is a repository that I used many years to create ready-to-go projects, each branch has differents apps ready to work.

    [my_django_template](https://github.com/christianmtr/my_django_template)

2. This is a small project used as microservice to save just one data that is used later with a django command.

    [fourabait](https://github.com/christianmtr/forabait) (it's a private repo, will change it to public.)

3. This is a very old project, I think it's not finished.

    [shoping_cart](https://github.com/christianmtr/shopping_cart)

4. A javascript project that we used in a react app, it's based in another module.

    [react module](https://github.com/christianmtr/react-simple-range-slider)

5. Code examples:

    - [Botkik's API file](https://gitlab.com/christianmtr/new-developer_v3/-/blob/main/answers/three_file_one.py)
    - [Bootkik's custom migration](https://gitlab.com/christianmtr/new-developer_v3/-/blob/main/answers/three_file_two.py)
    - [Bootkik's mobile app screen writen in React Native](https://gitlab.com/christianmtr/new-developer_v3/-/blob/main/answers/three_file_three.js)
    - Some bash scripts made to create backups.

        ```
        #!/bin/bash

        BUCKET_NAME="enterprise-database-backup"
        S3_BUCKET_URL="s3://${BUCKET_NAME}"
        WEEK_AGO=$(date -d "week ago" +%F)

        echo "Getting objects list..."
        OBJECT_LIST=$(s3cmd ls ${S3_BUCKET_URL})

        echo "Checking dates..."
        for item in "${OBJECT_LIST}"
        do
            object_date=$(echo "${item}" | awk 'NR==1{print $1}')
            object_url=$(echo "${item}" | awk 'NR==1{print $4}')
            if [[ "${WEEK_AGO}" > "${object_date}" ]]
            then
                echo "it's old. Deleting ${object_url}."
                s3cmd del ${object_url}
            else
                echo "it's younger."
            fi
        done

        echo "That's all folks!"
        ```
        ```
        #!/bin/bash

        BACKUPS_DIR="~/backups/enterprise/"
        WEEK_AGO=$(date -d "week ago" +%F)

        echo "Getting files list..."
        FILES_LIST=$(ls ${BACKUPS_DIR})

        echo "Checking dates..."
        for file in ${FILES_LIST}
        do
            created_at=$(stat -c %w ${file})
            creation_date=$(echo "${created_at}" | awk 'NR==1{$1}')

            if [[ "${WEEK_AGO}" > "${creation_date}" ]]
            then
                file_path=${BACKUPS_DIR}+${file}
                echo "it's old. Deleting ${file}."
                rm -f ${file_path}
            else
                echo "it's younger."
            fi
        done

        echo "Tha's all folks!"
        ```
        ```
        #!/bin/bash

        APP='bootkik-enterprise'
        WORK_DIRECTORY='~/Proyectos/bootkik/buckups'

        DB_NAME='bootkik_develop'
        DB_USER='postgres'
        DB_PASS='123qweqweqwe'

        BUCKET_NAME='enterprise-database-backup'

        TIMESTAMP=$(TZ=America/Regina date +%F_%T%:z | tr ':' '-')
        TEMP_FILE=$(mktemp tmp.XXXXXXXXXX)
        S3_FILE="s3://$BUCKET_NAME/$APP-backup-$TIMESTAMP"
        LOCAL_FILE="$WORK_DIRECTORY/$APP-backup-$TIMESTAMP"

        PGPASSWORD=$DB_PASS pg_dump -Fc --no-acl --encoding "utf-8" --format=d -h localhost -U $DB_USER $DB_NAME --file "${LOCAL_FILE}"
        zip ${LOCAL_FILE}.zip ${LOCAL_FILE}
        s3cmd put ${LOCAL_FILE}.zip $S3_FILE --no-encrypt

        rm -rf $LOCAL_FILE

        echo "That's all folks!"
        ```
