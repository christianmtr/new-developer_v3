## 4

>___Please, write a code or pseudocode that solves the problem in which I have a 
collection of numbers in ascending order. You need to find the matching pair 
that it is equal to a sum that its also given to you. If you make any 
assumption, let us know.___

>___Example:___
* [2,3,6,7]  sum = 9  - OK, matching pair (3,6)
* [1,3,3,7]  sum = 9  - No
* Consider recibe 1 000 000 numbers



I have two aproachs to solve this.

This one will return the first pair that sums the given number. Should receive a list of numbers and the target.
```
def find_pair(numbers, target):
    left, right = 0, len(numbers) - 1
    
    while left < right:
        current_sum = numbers[left] + numbers[right]
        if current_sum == target:
            return (numbers[left], numbers[right])
        elif current_sum < target:
            left += 1
        else:
            right -= 1
    
    return "no"

# example
find_pair([2, 3, 6, 7], 9)
> (2, 7)
```

This one will return all pairs that sums the given target.
```
def find_pairs(numbers, target):
    left, right = 0, len(numbers) - 1
    pairs = []
    
    while left < right:
        current_sum = numbers[left] + numbers[right]
        if current_sum == target:
            pairs.append((numbers[left], numbers[right]))
            left += 1 # Podemos mover ambos punteros para encontrar más pares
            right -= 1
        elif current_sum < target:
            left += 1
        else:
            right -= 1
    
    return pairs if pairs else "No"

# example
find_pairs([2, 3, 6, 7], 9)
> [(2, 7), (3, 6)]
```

The scripts can be tested on [Google Colaboratory](https://colab.research.google.com/drive/17f-s9AtiyS9B6ZSnxp8tEk9gxsUAGJXv?usp=sharing), [here](https://colab.research.google.com/drive/17f-s9AtiyS9B6ZSnxp8tEk9gxsUAGJXv#scrollTo=VJ06AR5ONfgS&line=6&uniqifier=1).
