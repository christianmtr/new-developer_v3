>___Write a program in the language of your choice that can accomplish this.
You can use Pseudocode.___
>___If you are not familiar with writing these programs, you can explain the
most representative concepts.___


I made some configurations for Circle CI, Travis CI and Github Actions.

For example:

```
name: scp pipeline
on:
  push:
    branches:
      - master

env:
  TARGET_DIR: /home/ubuntu/acopiadora

jobs:
  build:
    name: Build
    runs-on: ubuntu-latest

    steps:
      - name: checkout
        uses: actions/checkout@v1

      - name: scp pipeline
        uses: cross-the-world/scp-pipeline@master
        with:
          host: ${{ secrets.SSH_TARGET_HOST }}
          user: ${{ secrets.SSH_USER }}
          pass: ${{ secrets.SSH_PASSWORD }}
          port: 22
          connect_timeout: 30s
          scp: |
            '.' => $TARGET_DIR
```

```
# Python CircleCI 2.0 configuration file
#
# Check https://circleci.com/docs/2.0/language-python/ for more details
# Script by @crowicked (Ante Beslic)
#
version: 2
jobs:
  build-develop:
    docker:
      - image: circleci/python:3.7.4

    working_directory: ~/repo
    
    steps:
      - checkout
      
      - restore_cache:
          keys:
            - v1-dependencies-{{ checksum "requirements.txt" }}
            # fallback to using the latest cache if no exact match is found
            - v1-dependencies-
      
      - run:
          name: Deploy & run staging server
          command: echo "Updating DEVELOP backend..." && ssh -o "StrictHostKeyChecking no" $SSH_USER@$SSH_HOST '~/UPDATE_SCRIPTS/dev_enterprise.sh'
      
      - save_cache:
          paths:
            - /home/ubuntu/dev-enterprise-backend/venv
          key: v1-dependencies-{{ checksum "requirements.txt" }}
  deploy-production:
    docker:
      - image: circleci/python:3.7.4

    working_directory: ~/repo
    
    steps:
      - checkout
      
      - run:
          name: Installing deployment dependencies
          command: pip install awsebcli --upgrade --user

      - run:
          name: Making AWS profile
          command: |
            mkdir  ~/.aws
            echo -e "[profile eb-cli]\naws_access_key_id=$AWS_ACCESS_KEY_ID\naws_secret_access_key=$AWS_SECRET_ACCESS_KEY\n" > ~/.aws/config

      - run:
          name: Deploying
          command: eb deploy --profile eb-cli
      
workflows:
  version: 2
  build-and-deploy:
    jobs:
    - build-develop:
        filters:
          branches:
            only:
            - develop
    - deploy-production:
        filters:
          branches:
            only:
            - master
```
Some bash scripts made to create backups.

```
#!/bin/bash

BUCKET_NAME="enterprise-database-backup"
S3_BUCKET_URL="s3://${BUCKET_NAME}"
WEEK_AGO=$(date -d "week ago" +%F)

echo "Getting objects list..."
OBJECT_LIST=$(s3cmd ls ${S3_BUCKET_URL})

echo "Checking dates..."
for item in "${OBJECT_LIST}"
do
    object_date=$(echo "${item}" | awk 'NR==1{print $1}')
    object_url=$(echo "${item}" | awk 'NR==1{print $4}')
    if [[ "${WEEK_AGO}" > "${object_date}" ]]
    then
        echo "it's old. Deleting ${object_url}."
        s3cmd del ${object_url}
    else
        echo "it's younger."
    fi
done

echo "That's all folks!"
```
```
#!/bin/bash

BACKUPS_DIR="~/backups/enterprise/"
WEEK_AGO=$(date -d "week ago" +%F)

echo "Getting files list..."
FILES_LIST=$(ls ${BACKUPS_DIR})

echo "Checking dates..."
for file in ${FILES_LIST}
do
    created_at=$(stat -c %w ${file})
    creation_date=$(echo "${created_at}" | awk 'NR==1{$1}')

    if [[ "${WEEK_AGO}" > "${creation_date}" ]]
    then
        file_path=${BACKUPS_DIR}+${file}
        echo "it's old. Deleting ${file}."
        rm -f ${file_path}
    else
        echo "it's younger."
    fi
done

echo "Tha's all folks!"
```
```
#!/bin/bash

APP='bootkik-enterprise'
WORK_DIRECTORY='~/Proyectos/bootkik/buckups'

DB_NAME='bootkik_develop'
DB_USER='postgres'
DB_PASS='123qweqweqwe'

BUCKET_NAME='enterprise-database-backup'

TIMESTAMP=$(TZ=America/Regina date +%F_%T%:z | tr ':' '-')
TEMP_FILE=$(mktemp tmp.XXXXXXXXXX)
S3_FILE="s3://$BUCKET_NAME/$APP-backup-$TIMESTAMP"
LOCAL_FILE="$WORK_DIRECTORY/$APP-backup-$TIMESTAMP"

PGPASSWORD=$DB_PASS pg_dump -Fc --no-acl --encoding "utf-8" --format=d -h localhost -U $DB_USER $DB_NAME --file "${LOCAL_FILE}"
zip ${LOCAL_FILE}.zip ${LOCAL_FILE}
s3cmd put ${LOCAL_FILE}.zip $S3_FILE --no-encrypt

rm -rf $LOCAL_FILE

echo "That's all folks!"
```
