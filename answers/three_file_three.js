import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  FlatList,
  ActivityIndicator,
} from "react-native";
import {
  BLACK_COLOR,
  CUSTOM_FONTS,
  GREY_COLOR,
  WHITE_COLOR,
  GREY_SECONDARY,
  PRIMARY_COLOR,
  RED_PRIMARY,
  GOLD_COLOR,
  GREEN_PRIMARY,
  DARK_COLOR,
} from "../../../styles/common";
import RecipeItem from "./RecipeItem";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import Icons from "../../../components/icons/IconSvg";
import { connect } from "react-redux";
import {
  fetchRecipes,
  fetchTasks,
  fetchTasksByGoalId,
  requestFetchRecipes,
  setActiveRecipe,
  checkTask,
  receiveFilterBy,
} from "../../../../services/actions/actionRecipes";
import { fetchCookbooks } from "../../../../services/actions/actionCookbooks";
import {
  fetchCompleteTask,
  receiveTask,
} from "./../../../../services/actions/actionTaskDetail";
import { fetchNotifications } from "./../../../../services/actions/actionsNotifications";
import _ from "lodash";
import Tooltip from "../../../components/tooltip/Tooltip";
import { ButtonRectanglePrimaryNormal } from "../../../components/buttonRectangle";
import customStorage from "../../../../services/session/custom-storage";
import TaskItem from "./TaskItem";
import BootkikStatusBar from "../../../components/BootkikStatusBar";

import amplitude from "../../../utils/amplitude";
import TabBarIcon from "../../../components/TabBarIcon";
import {
  addNewNotification,
  markSeenNotifications,
} from "./../../../../services/actions/actionsNotifications";
import {
  notificationDisplayedListener,
  notificationListener,
  notificationOpenedListener,
  loadConfigurationNotifications,
} from "../../../../services/notifications/configuration";
class Recipes extends Component {
  static navigationOptions = {
    title: "Recipes",
    tabBarIcon: ({ focused, tintColor }) => {
      return (
        <TabBarIcon
          MainElement={Icons.clipboardList({
            svg: { width: 20, height: 20 },
            path: { fill: tintColor },
          })}
          focused={focused}
          routeName={"Recipes"}
        />
      );
    },
  };

  constructor(...props) {
    super(...props);

    this.state = {
      viewGallery: true,
      loading: true,
      sortBy: "all",
      user: null,
      current_goal_id: -1,
      current_recipe_name: "",
      showTooltip: false,
    };

    this.showRecipe = this.showRecipe.bind(this);
    this.renderRecipes = this.renderRecipes.bind(this);
    this._renderRecipe = this._renderRecipe.bind(this);
    this._renderTaksItem = this._renderTaksItem.bind(this);
    this.renderTaskList = this.renderTaskList.bind(this);
  }

  componentDidMount() {
    amplitude.logEvent("Load_Home");

    // If this is the first time they are logging into the application,
    // We want to redirect them to the Discover page
    // (might need to move this logic to home.js instead, while keeping the nav)
    const firstLogin = this.props.navigation.getParam("firstLogin", false);
    if (firstLogin === true) {
      this.props.navigation.navigate("Discover");
    }
    loadConfigurationNotifications().then(() => {
      this.notificationOpenedListener = notificationOpenedListener(
        this.props.navigation
      );
      this.notificationListener = notificationListener(
        this.props.addNewNotification
      );
    });
    this.willFocusListener = this.props.navigation.addListener(
      "willFocus",
      this.reselectRecipe
    );
    this.setTooltipTask();
    this.props.fetchNotifications();

    customStorage
      .retrieveUserInfo("userInfo")
      .then((userInfo) => {
        this.setState({ user: userInfo }, () => {
          this.fetchRecipes();
          this.props.fetchCookbooks();
        });
      })
      .catch((error) => {
        console.log("err", error);
      });
  }

  componentDidUpdate(prevProps) {
    const { haveCompletedRecipe, completedRecipes, navigation, activeRecipe } =
      this.props;

    if (haveCompletedRecipe && !prevProps.haveCompletedRecipe) {
      navigation.navigate({
        routeName: "CompletedRecipe",
        params: {
          recipies: completedRecipes,
          user: this.state.user,
        },
      });
    }

    if (activeRecipe.id !== prevProps.activeRecipe.id) {
      this.scrollToActiveRecipe();
    }
  }

  componentWillUnmount() {
    this.willFocusListener.remove();
    notificationDisplayedListener();
    notificationListener();
    this.notificationOpenedListener();
  }

  fetchRecipes = () => {
    const { fetchRecipes } = this.props;
    const { user } = this.state;

    fetchRecipes({
      id: user.user_id,
      page: 1,
      setDefaultTasks: true,
    }).then(() => {
      this.setState({
        loading: false,
      });

      if (this.props.recipes.length !== 0) {
        this.selectRecipe(this.props.recipes[0]);
      }
    });
  };

  scrollToActiveRecipe = () => {
    const { activeRecipe, recipes } = this.props;
    const recipeIndex = _.findIndex(recipes, (recipe) => {
      return recipe.goal.id === activeRecipe.id;
    });

    if (recipeIndex === -1) {
      return null;
    }

    this.flatListRef.scrollToIndex({
      animated: true,
      index: recipeIndex,
    });
  };

  reselectRecipe = () => {
    const { recipes } = this.props;
    const { current_goal_id } = this.state;

    if (recipes.length !== 0 && current_goal_id === -1) {
      this.selectRecipe(recipes[0]);
    }
  };

  navigateToAllList = () => {
    const { cookbooks, recipes, navigation } = this.props;

    navigation.navigate("RecipeCookbookList", {
      cookbooks,
      recipes,
    });
  };

  getRecipeLayoutInfo = (data, index) => {
    // This is the item length which is calculated from RecipeItem.style.js
    // 230 - width, 20 - margin
    const itemLength = 250;

    return {
      length: itemLength,
      index,
      offset: itemLength * index,
    };
  };

  setRecipeListRef = (ref) => (this.flatListRef = ref);

  setTooltipTask = () => {
    customStorage.retrieveItem(`recipe-tooltip`).then((tooltip) => {
      this.setState({
        showTooltip: _.isNil(tooltip),
      });
    });
  };

  calculateStateLoadMoreTask = (tasks) => {
    let state = true;
    tasks = tasks.filter((task) => {
      if (task.is_completed) {
        if (this.props.filterBy !== "completed_tasks") {
          return false;
        }
      } else {
        if (this.props.filterBy === "completed_tasks") {
          return false;
        }
      }
      return true;
    });
    console.log("Tasks filtraditos: ", tasks);
    if (tasks.length <= 7) {
      state = false;
    }
    this.setState({
      showLoadMore: state,
    });
  };

  selectRecipe = (item, clicked) => {
    let { current_goal_id } = this.state;
    if (current_goal_id == item.goal.id && clicked) {
      // check if the clicked thumbnail is already selected
      let tasks = this.props.tasks;
      for (let i = 0; i < tasks.length; i++) {
        let itemm = tasks[i];
        if (itemm.is_completed == false) {
          this.props.navigation.navigate({
            routeName: "TaskDetail",
            params: {
              task: itemm,
              user: this.state.user.user_id,
              allTasks: tasks,
              index: i,
            },
            key: `TaskDetail${item.id}`,
          });
          break;
        }
      }
    } else {
      this.setState(
        {
          current_goal_id: item.goal.id,
          current_recipe_name: item.goal.name,
        },
        () => {
          // let recipe = this.props.getRecipeById(item.goal.id);
          this.props.setActiveRecipe({
            id: item.goal.id,
            name: item.goal.name,
          });
          this.props.fetchTasksByGoalId({
            id: item.goal.id,
            callback: (tasks) => {
              //console.log('Holii de ',item.goal.name,tasks);
              //this.calculateStateLoadMoreTask(tasks);
              //console.log('this.props.idTaskSelected:_', this.props.idTaskSelected);
              //if (this.props.idTaskSelected) {
              //  this.props.receiveTask(this.props.idTaskSelected);
              //} else {
              //  let task = tasks.filter(item => !item.is_completed)[0];
              //this.props.handleChangeTaskDetail(task);
              //}
            },
          });
        }
      );
    }
  };

  showRecipe(value) {
    this.setState({ viewGallery: !value });
  }

  _extractKeyForRecipe = (item) => item.id.toString();

  _renderRecipe({ item, index }) {
    return (
      <RecipeItem
        data={item.goal}
        index={index}
        even={true}
        onPressRecipe={() => {
          this.selectRecipe(item, true);
          // this.props.setActiveRecipe({
          //   id: item.goal.id,
          //   name: item.goal.name
          // });
          this.props.fetchTasksByGoalId({ id: item.goal.id });
          /*if (item.goal.id !== 'show_all') {
          } else {
            this.props.fetchTasks();
          }*/
        }}
      />
    );
  }

  renderRecipes() {
    if (!this.state.viewGallery || this.props.isFetching) {
      return;
    }

    const recipes = this.props.recipes.slice();

    return (
      <View style={{ height: hp("33%") }}>
        <FlatList
          data={recipes}
          horizontal
          getItemLayout={this.getRecipeLayoutInfo}
          ref={this.setRecipeListRef}
          showsHorizontalScrollIndicator={false}
          renderItem={this._renderRecipe}
          keyExtractor={this._extractKeyForRecipe}
        />
      </View>
    );
  }

  renderTaskList() {
    if (this.props.tasks.length !== 0) {
      return (
        <ScrollView>
          <FlatList
            data={this.props.tasks.filter((item, index) => {
              if (item.case_study === null) {
                item.key = `${index}`;
                return item;
              }
            })}
            showsVerticalScrollIndicator={false}
            renderItem={this._renderTaksItem}
          />
        </ScrollView>
      );
    }
  }

  _renderTaksItem({ item, index }) {
    const { user } = this.state;
    const { navigation, tasks } = this.props;

    return (
      <TaskItem
        taskId={item.id}
        selectRecipe={this.selectRecipe}
        user={user.user_id}
        navigation={navigation}
        index={index}
        tasks={tasks}
      />
    );
  }

  _renderViewAll = () => {
    const { recipes, cookbooks } = this.props;
    const moreThan4Recipes = recipes.length > 4;

    if (!moreThan4Recipes && _.isEmpty(cookbooks)) {
      return null;
    }

    return (
      <View style={styles.headerActions}>
        <TouchableOpacity
          style={styles.headerAction}
          onPress={this.navigateToAllList}
        >
          <Text style={styles.action}>View All</Text>
        </TouchableOpacity>
      </View>
    );
  };

  _renderTooltip = () => {
    const { showTooltip } = this.state;

    if (!showTooltip) {
      return null;
    }

    return <Tooltip type={"recipe"} withNavigation={true} />;
  };

  render() {
    let { loading } = this.state;

    return (
      <View style={styles.mainContainer}>
        <BootkikStatusBar
          backgroundColor={WHITE_COLOR}
          barStyle="dark-content"
        />
        <View style={styles.header}>
          <View style={[styles.headerWrap, styles.headerWrapWithActions]}>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                alignItems: "flex-start",
              }}
            >
              <View style={styles.headerText}>
                <Text
                  style={{
                    fontSize: 23,
                    color: DARK_COLOR,
                    fontFamily: CUSTOM_FONTS["FONT_BOLD"],
                  }}
                >
                  ADDED RECIPES
                </Text>
              </View>
              <TouchableOpacity
                style={styles.arrowHeaderWrap}
                onPress={() => this.showRecipe(this.state.viewGallery)}
              >
                {this.state.viewGallery
                  ? Icons.arrowRight({
                      svg: { width: 30, height: 30 },
                      path: {
                        fill: GREY_COLOR,
                      },
                    })
                  : Icons.arrowDown({
                      svg: { width: 30, height: 30 },
                      path: {
                        fill: GREY_COLOR,
                      },
                    })}
              </TouchableOpacity>
            </View>
            {this._renderViewAll()}
          </View>
        </View>
        {(this.props.isFetching && this.props.recipes.length === 0) ||
        loading ? (
          <View style={[styles.activatorWrap, styles.emptyRecipesFirst]}>
            <ActivityIndicator size="large" color={BLACK_COLOR} />
          </View>
        ) : !this.props.isFetching && this.props.recipes.length === 0 ? (
          <View style={[styles.emptyRecipes, styles.emptyRecipesFirst]}>
            <Text style={styles.emptyRecipesText}>
              Recipes are business milestones that experts help you achieve! Add
              your first recipe to get started!
            </Text>
            <ButtonRectanglePrimaryNormal
              styleButton={styles.btnStyle}
              contentStyle={styles.btnStyleContent}
              text={"DISCOVER"}
              onPress={() => {
                this.props.navigation.navigate("Discover");
              }}
            />
          </View>
        ) : (
          this.renderRecipes()
        )}
        <View style={[styles.header, styles.secondHeader]}>
          <View style={[styles.headerWrap, styles.headerWrapWithActions]}>
            <View
              style={{
                flex: 1,
                flexDirection: "column",
                alignItems: "flex-start",
              }}
            >
              <View style={styles.headerText}>
                <Text
                  style={{
                    fontSize: 23,
                    color: DARK_COLOR,
                    fontFamily: CUSTOM_FONTS["FONT_BOLD"],
                  }}
                >
                  STEPS TO:
                </Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={[
                    this.props.activeRecipe.id !== -1
                      ? styles.fontTextActive
                      : {},
                  ]}
                >
                  {this.props.activeRecipe.name.toUpperCase()}
                </Text>
              </View>
            </View>
            {/* <View style={styles.headerActions}>
              <TouchableOpacity
                style={styles.headerAction}
                onPress={() => this.props.navigation.navigate('SortRecipe')}
              >
                {Icons.configIcon({
                  svg: { width: 24, height: 21 },
                  path: {
                    fill: GREEN_PRIMARY,
                    stroke: GREEN_PRIMARY,
                    strokeWidth: '0.5',
                  },
                })}
              </TouchableOpacity>
              </View> */}
          </View>
        </View>

        {(this.props.isFetching && this.props.tasks.length === 0) || loading ? (
          <View style={styles.activatorWrap}>
            <ActivityIndicator size="large" color={BLACK_COLOR} />
          </View>
        ) : !this.props.isFetching && this.props.tasks.length === 0 ? (
          <View style={styles.emptyRecipes}>
            <Text style={styles.emptyRecipesText}>
              Once you add your first recipe, all its sub-tasks appear here. Let
              us worry about the highway from dream to reality, you just
              identify where you want to go!
            </Text>
            <ButtonRectanglePrimaryNormal
              styleButton={styles.btnStyle}
              contentStyle={styles.btnStyleContent}
              text={"DISCOVER"}
              onPress={() => {
                this.props.navigation.navigate("Discover");
              }}
            />
          </View>
        ) : (
          this.renderTaskList()
        )}

        {this._renderTooltip()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: WHITE_COLOR,
    flex: 1,
  },
  header: {
    marginTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    flexDirection: "row",
  },
  secondHeader: {
    marginTop: 10,
  },
  headerText: {
    paddingTop: 10,
    flexDirection: "row",
    borderTopWidth: 1,
    borderTopColor: DARK_COLOR,
  },
  fontText: {
    color: DARK_COLOR,
    fontSize: 14,
    fontFamily: CUSTOM_FONTS["FONT_BOLD"],
    paddingBottom: 10,
  },
  fontTextActive: {
    color: PRIMARY_COLOR,
    fontSize: 14,
    fontFamily: CUSTOM_FONTS.FONT_BOLD,
    fontWeight: "700",
    paddingBottom: 10,
  },
  headerWrap: {
    flexDirection: "row",
    alignItems: "center",
  },
  headerWrapWithActions: {
    flex: 1,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
  },
  headerActions: {
    paddingTop: 20,
    flexDirection: "row",
  },
  headerAction: {
    flexDirection: "row",
    alignItems: "center",
  },
  headerActionText: {
    fontSize: 11,
    fontFamily: CUSTOM_FONTS["FONT_LIGHT"],
    marginRight: 5,
  },
  headerActionLeft: {
    borderRightColor: "#C7C7C7",
    paddingRight: 10,
    marginRight: 10,
    borderRightWidth: 1,
  },
  headerActionIcon: {
    color: GOLD_COLOR,
  },
  headerActionIconReverse: {
    transform: [{ rotate: "90deg" }],
  },
  arrowHeaderWrap: {
    marginLeft: 15,
    marginTop: 10,
    transform: [{ translateY: 5 }],
  },
  arrowHeader: {
    color: GREY_COLOR,
  },
  viewGallery: {
    //paddingLeft: -11,
  },
  taskItem: {
    minHeight: 50,
    paddingLeft: 10,
    paddingRight: 20,
    flexDirection: "row",
    alignItems: "center",
  },
  taskItemCol: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    flex: 1,
    height: "100%",
    borderBottomWidth: 1,
    borderBottomColor: GREY_SECONDARY,
    marginLeft: 10,
  },
  taskItemTitle: {
    fontSize: 14,
    maxWidth: wp("60%"),
    color: BLACK_COLOR,
    fontFamily: CUSTOM_FONTS["FONT_LIGHT"],
  },
  taskItemDate: {
    fontSize: 10,
    fontFamily: CUSTOM_FONTS["FONT_LIGHT"],
    color: RED_PRIMARY,
  },
  emptyRecipes: {
    padding: 20,
  },
  emptyRecipesFirst: {
    height: hp("33%"),
  },
  emptyRecipesText: {
    fontFamily: CUSTOM_FONTS["FONT_LIGHT"],
    fontStyle: "italic",
    color: BLACK_COLOR,
    fontSize: 10,
  },
  action: {
    color: GREEN_PRIMARY,
  },
  btnStyle: {
    width: "30%",
    marginTop: 10,
  },
  btnStyleContent: {},
  activatorWrap: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
  completeTaskColor: {
    color: GREEN_PRIMARY,
    fontFamily: CUSTOM_FONTS["FONT_NORMAL"],
  },
  iconsTask: {
    alignItems: "flex-end",
  },
  checks: {
    padding: 10,
  },
});

const mapStateToProps = ({ recipes, cookbooks }) => {
  let getRecipeById = (idRecipe) => {
    let recipe = null;

    for (let i = 0; i < recipes.recipes.length; i++) {
      let recipeA = recipes.recipes[i];
      if (recipeA.goal.id === idRecipe) {
        recipe = recipeA;
      }
    }

    return recipe;
  };

  return {
    activeRecipe: recipes.activeRecipe,
    cookbooks: cookbooks.cookbooks,
    getRecipeById,
    recipes: recipes.recipes,
    tasks: recipes.tasks,
    sortBy: recipes.sortBy,
    isFetching: recipes.isFetching,
    nextPage: recipes.nextPage,
    count: recipes.count,
    morePage: recipes.morePage,
    filterBy: recipes.filterBy,
    haveCompletedRecipe: recipes.haveCompletedRecipe,
    completedRecipes: recipes.completedRecipes,
  };
};
export default connect(mapStateToProps, {
  fetchCookbooks,
  fetchRecipes,
  fetchTasks,
  fetchTasksByGoalId,
  requestFetchRecipes,
  fetchCompleteTask,
  receiveFilterBy,
  setActiveRecipe,
  receiveTask,
  checkTask,
  fetchNotifications,
  addNewNotification,
})(Recipes);
