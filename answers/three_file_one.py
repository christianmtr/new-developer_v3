import json
import time
from datetime import datetime

import stripe
from django.conf import settings
from django.core.exceptions import MultipleObjectsReturned
from django.db.models import Q
from django.utils.text import slugify
from drf_yasg.utils import swagger_auto_schema
from elasticsearch_dsl import Search, connections
from rest_framework import status, generics, viewsets
from rest_framework.decorators import action, api_view, permission_classes
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.views import TokenObtainPairView
from tenant_schemas.postgresql_backend.base import _is_valid_schema_name

from bootkik_enterprise_backend.settings import ELASTICSEARCH_HOST, ANNUAL_PLAN_REFUND_PERIOD
from content.models import Recipe
from core.api.permissions import WeArePartners, ImAdminOrManager, ImAdmin, ImBillingOwner, MemberCanSeeMember
from core.api.serializers import UserSerializer, OrganizationSerializer, CustomTokenObtainPairSerializer, \
    ChangePasswordSerializer, BaseUserSerializer, UserOrganizationDetailedSerializer, SingleUserSerializer, \
    RequestInvitationSerializer, PublicOrganizationSerializer, UserForSearch, \
    UserPermissionCloneSerializer, UserPermissionDetailedCloneSerializer
from core.api.swagger_serializers import GuestRequest, MessageResponse, ValidateGuestRequest, \
    ResetPasswordRequestRequest, ResetPasswordRequest, ValidateOrganizationNameRequest, CreateTeamMemberRequest, \
    AddCreditCardRequest, CreditCardResponse
from core.email_utils import send_welcome_message, send_reset_password_message, send_invitation_message, \
    send_request_invitation_message, send_organization_login, send_invalid_email, send_message_admin_to_upgrade
from core.firebase_utils import send_remove_notification
from core.models import Guest, User, Token, Organization, UserOrganization
from core.sendgrid_utils import remove_email_from_contact_list, NO_MEMBER_LIST, ADMINS_LIST
from core.slack_utils import compose_email_failed_message, compose_add_credit_card, compose_change_plan_message, \
    compose_created_organization_message, compose_invited_to_organization_message, compose_email_sent_message
from core.stripe_utils import handle_invoice_event, handle_customer_event

ALLOWED_STATUS = ['bounce', 'dropped']
PLAN_IDS = {
    'year': settings.STRIPE_ANNUAL_PLAN_ID,
    'month': settings.STRIPE_MONTHLY_PLAN_ID,
}
stripe.api_key = settings.STRIPE_SECRET_KEY


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer


@swagger_auto_schema(
    method='POST',
    operation_description="Take an email and registers it as user. Sends an email with link to create a user",
    request_body=GuestRequest,
    responses={201: MessageResponse},
)
@api_view(['POST'])
@permission_classes((AllowAny,))
def create_user_as_guest(request):
    try:
        request_body = request.body
        json_body = json.loads(request_body)
        email = json_body['email'].lower()
        resend = bool(json_body['resend'])

        guest, created = Guest.objects.get_or_create(email=email)

        User.objects.get(email=email)
    except User.DoesNotExist:
        if created:
            send_welcome_message(guest)
            return Response({'message': '%s saved as Guest.' % email}, status=status.HTTP_201_CREATED)
        else:
            if resend:
                send_welcome_message(guest)
                return Response({'message': 'Message was sent.'}, status=status.HTTP_200_OK)
            else:
                return Response({'message': 'Customer already exists'}, status=status.HTTP_406_NOT_ACCEPTABLE)
    except KeyError:
        return Response({'message': 'Given fields are not valid.'}, status=status.HTTP_406_NOT_ACCEPTABLE)
    else:
        return Response({'message': 'User with this email has already registered'}, status=status.HTTP_409_CONFLICT)


@swagger_auto_schema(
    method='POST',
    operation_description="Validate the Guest token and email before showing the new user form",
    request_body=ValidateGuestRequest,
    responses={201: MessageResponse},
)
@api_view(['POST'])
@permission_classes((AllowAny,))
def validate_guest_token(request):
    try:
        request_body = request.body
        json_body = json.loads(request_body)
        guest_email = json_body['guest_email']
        guest_token = json_body['guest_token']

        User.objects.get(email=guest_email)
    except User.DoesNotExist:
        try:
            guest = Guest.objects.get(email=guest_email, token=guest_token)
        except Guest.DoesNotExist:
            return Response({'message': 'Guest does not exists.'}, status=status.HTTP_404_NOT_FOUND)
        else:
            current_token = guest.token
            if current_token.__str__() == guest_token.__str__():
                return Response({'message': 'Token is correct'}, status=status.HTTP_202_ACCEPTED)
            else:
                return Response({'message': 'Token is not correct.'}, status=status.HTTP_406_NOT_ACCEPTABLE)
    except KeyError:
        return Response({'message': 'Given fields are not valid.'}, status=status.HTTP_406_NOT_ACCEPTABLE)
    else:
        return Response({'message': 'Email already in use.'}, status=status.HTTP_409_CONFLICT)


@swagger_auto_schema(
    method='POST',
    operation_description="Send an email to the user with reset password url",
    request_body=ResetPasswordRequestRequest,
    responses={201: MessageResponse},
)
@api_view(['POST'])
@permission_classes((AllowAny,))
def reset_password_request(request):
    try:
        request_body = request.body
        json_body = json.loads(request_body)
        email = json_body['email'].lower()

        user = User.objects.get(email=email)
        token, created = Token.objects.get_or_create(user=user, used=False)
    except User.DoesNotExist:
        return Response({'message': 'Email is not registered.'}, status=status.HTTP_404_NOT_FOUND)
    except MultipleObjectsReturned:
        return Response({'message': 'There are more than one active token.'})
    except KeyError:
        return Response({'message': 'Given fields are not valid.'}, status=status.HTTP_406_NOT_ACCEPTABLE)
    else:
        send_reset_password_message(token)
        return Response({'message': 'Message was sent.'}, status=status.HTTP_200_OK)


@swagger_auto_schema(
    method='POST',
    operation_description="Reset users password (validates that the token is correct before)",
    request_body=ResetPasswordRequest,
    responses={201: MessageResponse},
)
@api_view(['POST'])
@permission_classes((AllowAny,))
def token_validation_and_reset_password(request):
    try:
        request_body = request.body
        json_body = json.loads(request_body)
        email = json_body['email'].lower()
        token = json_body['token']
        password = json_body['password']

        user = User.objects.get(email=email)
        token = Token.objects.get(user=user, token=token, used=False)
    except User.DoesNotExist:
        return Response({'message': 'Email is not registered.'}, status=status.HTTP_404_NOT_FOUND)
    except Token.DoesNotExist:
        return Response(
            {'message': 'The given email does not have an active token, or the token has been used before.'},
            status=status.HTTP_406_NOT_ACCEPTABLE)
    else:
        token.used = True
        user.set_password(password)
        token.save()
        user.save()
        return Response({'message': 'Password changed'}, status=status.HTTP_200_OK)


@swagger_auto_schema(
    method='POST',
    operation_description="Validate that the organization name is unique",
    request_body=ValidateOrganizationNameRequest,
    responses={201: MessageResponse},
)
@api_view(['POST'])
@permission_classes((AllowAny,))
def validate_organization_name(request):
    try:
        request_body = request.body
        json_body = json.loads(request_body)
        name = json_body['name']
        domain_url = slugify(name)
        schema_name = domain_url.replace('-', '_')

        if name.lower() in settings.BLOCKED_NAMES:
            return Response({'message': 'Can not create an organization with the given name.'},
                            status=status.HTTP_406_NOT_ACCEPTABLE)

        if not _is_valid_schema_name(schema_name):
            schema_name = '_' + schema_name

        organization_query = Organization.objects.filter(Q(schema_name__iexact=schema_name) | Q(domain_url__iexact=domain_url) | Q(old_domains__contains=[domain_url]))
        if organization_query.count() == 0:		
            return Response({'message': 'Organization name is unique', 'domain_url': domain_url, 'schema_name': schema_name}, status=status.HTTP_202_ACCEPTED)
        else:
            return Response({'message': 'Organization name already in use.'}, status=status.HTTP_409_CONFLICT)        
    except Exception as e:
        return Response({'message': 'Organization name already in use.', 'error': e.__str__()}, status=status.HTTP_409_CONFLICT)


@swagger_auto_schema(
    method='POST',
    operation_description="Validate that the organization domain is unique",
    request_body=ValidateOrganizationNameRequest,
    responses={201: MessageResponse},
)
@api_view(['POST'])
@permission_classes((AllowAny,))
def validate_organization_domain(request):
    try: 
        request_body = request.body
        json_body = json.loads(request_body)
        domain = json_body['domain']

        current_organization = request.organization
        organizations = Organization.objects.filter(Q(domain_url__iexact=domain) | Q(old_domains__contains=[domain]))

        if organizations.count() == 0:		
            return Response({'message': 'Organization domain is unique'}, status=status.HTTP_202_ACCEPTED)
        else:
            first_organization = organizations[0]
            if first_organization == current_organization:
                return Response({'message': 'Subdomain belongs to this organization'}, status=status.HTTP_202_ACCEPTED)

            return Response({'message': 'Organization domain already in use.'}, status=status.HTTP_409_CONFLICT)
    except User.DoesNotExist:
        return Response({'message': 'User does not exists'}, status=status.HTTP_404_NOT_FOUND)


@swagger_auto_schema(
    method='POST',
    operation_description="Create a team member in your organization",
    request_body=CreateTeamMemberRequest,
    responses={201: MessageResponse},
)
@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def create_user_with_permissions(request):
    request_body = request.body
    json_body = json.loads(request_body)
    organization = request.organization
    new_members = []
    failed = []

    for email in json_body['emails']:
        try:
            e_mail = email.lower()
            token = None

            user, created = User.objects.get_or_create(
                email=e_mail, username=e_mail)
            if created:
                password = User.objects.make_random_password()
                user.set_password(password)
                token = Token.objects.create(user=user, used=False)

            if not user.first_name:
                token = Token.objects.get(user=user, used=False)

            user_organization, created_uo = UserOrganization.objects.get_or_create(user=user, organization=organization)

            if created_uo:
                user_organization.is_active = False
                user_organization.added_by = request.user
                user_organization.save()

            send_invitation_message(user, organization, token)
            compose_invited_to_organization_message(request.user, organization, user.email)
            new_members.append(user_organization)
        except Exception as e:
            if created:
                user.delete()
            
            if created_uo:
               user_organization.delete()

            failed.append(e_mail)

    serializer = UserOrganizationDetailedSerializer(new_members, many=True)

    return Response({'message': 'Users with permission created', 'users': serializer.data, 'failed': failed}, status=status.HTTP_201_CREATED)


@swagger_auto_schema(
    method='GET',
    operation_description="Activate the user in a organization.",
    responses={202: MessageResponse},
)
@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def activate_as_organization_member(request):
    current_user = request.user
    try:
        user_organization = UserOrganization.objects.get(
            user=current_user, organization=request.organization, is_active=False)
    except UserOrganization.DoesNotExist:
        if current_user.is_superuser:
            return Response({'message': 'The user was activated in this organization.',
                             'organization': OrganizationSerializer(request.organization).data},
                            status=status.HTTP_202_ACCEPTED)
        return Response({'message': 'Current user is already member or was not invited.'},
                        status=status.HTTP_404_NOT_FOUND)
    else:
        user_organization.is_active = True
        user_organization.save()
        return Response({'message': 'The user was activated in this organization.', 'organization': OrganizationSerializer(request.organization).data}, status=status.HTTP_202_ACCEPTED)


@swagger_auto_schema(
    method='POST',
    operation_description="Validate the member login before showing the onboard form",
    request_body=ValidateGuestRequest,
    responses={201: MessageResponse},
)
@api_view(['POST'])
@permission_classes((AllowAny,))
def validate_member_token(request):
    try:
        request_body = request.body
        json_body = json.loads(request_body)
        email = json_body['email']
        token = json_body['token']

        user = User.objects.get(email=email.lower())
        token = Token.objects.get(user=user, token=token, used=False)
    except User.DoesNotExist:
        return Response({'message': 'User doesnt exist'}, status=status.HTTP_406_NOT_ACCEPTABLE)
    except Token.DoesNotExist:
        return Response({'message': 'Token doesnt exist'}, status=status.HTTP_406_NOT_ACCEPTABLE)
    except KeyError:
        return Response({'message': 'Given fields are not valid.'}, status=status.HTTP_406_NOT_ACCEPTABLE)
    else:
        return Response({'message': 'Token is good'}, status=status.HTTP_200_OK)


@swagger_auto_schema(
    method='PATCH',
    operation_description="Change the organization name, using api_view function to avoid change any other field.",
    request_body=ValidateOrganizationNameRequest,
    responses={202: MessageResponse},
)
@api_view(['PATCH'])
@permission_classes((IsAuthenticated,))
def change_organization_name(request, organization_id):
    try:
        request_body = request.body
        json_body = json.loads(request_body)
        name = json_body['name']

        Organization.objects.get(id=organization_id)
    except Organization.DoesNotExist:
        return Response({'message': 'Organization does not exists'}, status=status.HTTP_404_NOT_FOUND)
    except KeyError:
        return Response({'message': 'Given fields are not valid.'}, status=status.HTTP_406_NOT_ACCEPTABLE)
    else:
        Organization.objects.filter(id=organization_id).update(name=name)
        return Response({'message': 'Organization name changed.'}, status=status.HTTP_202_ACCEPTED)


@swagger_auto_schema(
    method='PATCH',
    operation_description="Remove permission to the current user.",
    request_body=None,
    responses={202: MessageResponse},
)
@api_view(['PATCH'])
@permission_classes((IsAuthenticated,))
def remove_user_from_organization(request, user_id):
    try:
        logged_in_user = request.user
        user = User.objects.get(id=user_id)
        schema_name = request.META.get('HTTP_X_DTS_SCHEMA')
        organization = request.organization
    except User.DoesNotExist:
        return Response({'message': 'User does not exists'}, status=status.HTTP_404_NOT_FOUND)
    else:
        user.organization.remove(organization)
        # TODO: check if is necessary remove the row or change `is_active` flag.
        Recipe.objects.filter(author=user).update(author=logged_in_user)
        send_remove_notification(
            user, OrganizationSerializer(organization).data)

        user_organization = UserOrganization.objects.filter(user=user, permission='admin')
        if not user_organization.exists:
            remove_email_from_contact_list(user, ADMINS_LIST)
    return Response({'message': 'Organization removed. Permission disabled. Recipes re-assigned'},
                    status=status.HTTP_202_ACCEPTED)


@api_view(['POST'])
@permission_classes((AllowAny,))
def sendgrid_webhook(request):
    request_body = request.body
    json_body = json.loads(request_body)

    for email_request in json_body:
        if email_request['event'] in ALLOWED_STATUS:
            compose_email_failed_message(email_request)
            #Get failed user info using id
            failed_user = User.objects.get(email=email_request['email'])
            failed_user_org = UserOrganization.objects.get(user_id=failed_user.id)
            #add condition to check last login
            if(failed_user.last_login is None and failed_user.first_name is ""):
                sender_info = User.objects.get(id=failed_user_org.added_by_id)
                send_invalid_email(sender_info, email_request)
                compose_email_sent_message(sender_info, failed_user)
                # TODO: delete user from User table
                failed_user_org.delete()


    return Response(status=status.HTTP_200_OK)


@swagger_auto_schema(
    method='GET',
    operation_description="Get organization list.",
    responses={200: MessageResponse},
)
@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def my_organizations(request):
    try:
        current_user = request.user
        user_organizations = UserOrganization.objects.filter(user=current_user, is_active=True).values('organization')
        organizations = Organization.objects.filter(id__in=user_organizations)
    except Exception as e:
        return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)
    else:
        return Response(OrganizationSerializer(organizations, many=True).data)


@swagger_auto_schema(
    method='POST',
    operation_description="Add credit card to the organization stripe account",
    request_body=AddCreditCardRequest,
    responses={202: CreditCardResponse},
)
@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def add_credit_card(request, organization_id):
    try:
        request_body = request.body
        json_body = json.loads(request_body)
        user = request.user
        card_token = json_body['card_token']
        card_id = json_body['card_id']

        organization = Organization.objects.get(id=organization_id)
    except Organization.DoesNotExist:
        return Response({'message': 'Organization does not exists'}, status=status.HTTP_404_NOT_FOUND)
    else:
        try:
            if not organization.stripe_token:
                customer = stripe.Customer.create(
                    name=organization.name,
                    email=user.email,
                    description="Account for {}".format(organization.name),
                    source=card_token
                )
                organization.stripe_token = customer['id']
                organization.save()
            else:
                customer = stripe.Customer.retrieve(organization.stripe_token)
                stripe.Customer.modify(
                    organization.stripe_token,
                    email=user.email,
                    name=f"{user.first_name} {user.last_name}",
                    source=card_token
                )
            card = customer.sources.retrieve(card_id)
            organization.billing_owner = user
            organization.save()
        except stripe.error.CardError as e:
            body = e.json_body
            err = body.get('error', {})
            return Response(err, status=e.http_status)
        except stripe.error.StripeError as e:
            body = e.json_body
            err = body.get('error', {})
            return Response(err, status=e.http_status)
        except Exception as e:
            return Response({'error': False, 'message': e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            compose_add_credit_card(user, organization.name)
            return Response(
                {'message': 'Credit card added.', 'card': card},
                status=status.HTTP_202_ACCEPTED
            )


@swagger_auto_schema(
    method='GET',
    operation_description="Get subscription associated with organization",
    request_body=None,
    responses={202: CreditCardResponse},
)
@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_subscription(request):
        organization = request.organization

        if organization.subscription_id:
            account = stripe.Customer.retrieve(organization.stripe_token)
            subscription = stripe.Subscription.retrieve(organization.subscription_id)
            stripe_cards = stripe.Customer.retrieve(
                organization.stripe_token).sources.list(limit=1, object='card')
            card = stripe_cards['data'][0] if stripe_cards else None

            selected_info = {
                'balance': abs(account.balance),
                'status': subscription.status,
                'current_period_end': subscription.current_period_end,
                'current_period_start': subscription.current_period_start,
                'seat_count': subscription.quantity,
                'type': subscription.plan.interval,
                'credit_card': card,
                'trial_end': subscription.trial_end,
                'cancel_at_period_end': subscription.cancel_at_period_end,
            }

            return Response({'message': 'Subscription retrieved', 'info': selected_info})
        else:
            return Response({'message': 'Organization doesnt have a subscription info'},
                            status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_next_invoice(request):
        organization = request.organization

        if organization.subscription_id:
            try:
                next_invoice = stripe.Invoice.upcoming(customer=organization.stripe_token)

                return Response({'message': 'Invoice retrieved', 'next_invoice': next_invoice})
            except stripe.error.StripeError as e:
                body = e.json_body
                err = body.get('error', {})
                return Response(err, status=e.http_status)
        else:
            return Response({'message': 'Organization doesnt have a subscription info'},
                            status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def get_next_prorationed_invoice(request):
        request_body = request.body
        json_body = json.loads(request_body)
        seat_count = json_body['seat_count']
        type = json_body['type']
        behavior = json_body['behavior']
        organization = request.organization

        if organization.subscription_id:
            try:
                proration_date = int(time.time())
                subscription = stripe.Subscription.retrieve(organization.subscription_id)

                items=[{
                    'id': subscription['items']['data'][0].id,
                    'plan': PLAN_IDS[type],
                    'quantity': seat_count,
                }]

                if behavior == 'none':
                    invoice = stripe.Invoice.upcoming(
                        customer=organization.stripe_token,
                        subscription=organization.subscription_id,
                        subscription_items=items,
                        subscription_proration_behavior=behavior,
                    )
                else:
                    invoice = stripe.Invoice.upcoming(
                        customer=organization.stripe_token,
                        subscription=organization.subscription_id,
                        subscription_items=items,
                        subscription_proration_behavior=behavior,
                        subscription_proration_date=proration_date,
                    )

                return Response({'message': 'Invoice retrieved', 'next_invoice': invoice})
            except stripe.error.StripeError as e:
                body = e.json_body
                err = body.get('error', {})
                return Response(err, status=e.http_status)
        else:
            return Response({'message': 'Organization doesnt have a subscription info'},
                            status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def change_plan(request):
    request_body = request.body
    json_body = json.loads(request_body)
    seat_count = json_body['seat_count']
    type = json_body['type']
    behavior = json_body['behavior']
    organization = request.organization

    if organization.subscription_id:
        try:
            subscription = stripe.Subscription.retrieve(organization.subscription_id)

            stripe.Subscription.modify(
                organization.subscription_id,
                cancel_at_period_end=False,
                proration_behavior=behavior,
                items=[{
                    'id': subscription['items']['data'][0].id,
                    'plan': PLAN_IDS[type],
                    'quantity': seat_count,
                }]
            )

            # NOTE: if you are moving to the free plan and you having pending charges, just mark them as uncollectible
            if seat_count < 3 and (subscription.status == 'past_due' or subscription.status == 'unpaid'):
                stripe.Invoice.mark_uncollectible(
                    subscription.latest_invoice,
                )

            return Response({
                'message': 'Updated the organization plan'}, 
                status=status.HTTP_202_ACCEPTED
            )
        except stripe.error.StripeError as e:
            body = e.json_body
            err = body.get('error', {})
            return Response(err, status=e.http_status)
    else:
        return Response({'message': 'Organization doesnt have a subscription info'},
                        status=status.HTTP_404_NOT_FOUND)


@swagger_auto_schema(
    method='GET',
    operation_description="Get credit card associated with organization",
    request_body=None,
    responses={202: CreditCardResponse},
)
@api_view(['GET'])
@permission_classes((IsAuthenticated, ImAdmin,))
def get_credit_card(request, organization_id):
    try:
        organization = Organization.objects.get(id=organization_id)
    except Organization.DoesNotExist:
        return Response({'message': 'Organization does not exists'}, status=status.HTTP_404_NOT_FOUND)
    else:
        if organization.stripe_token:
            cards = stripe.Customer.retrieve(
                organization.stripe_token).sources.list(limit=1, object='card')
            cards_arr = [card for card in cards['data']]

            if not cards_arr:
                return Response({'message': 'Organization has no credit cards'},
                                status=status.HTTP_404_NOT_FOUND)

            return Response({'message': 'Credit cards retrieved', 'card': cards_arr[0], 'subscription_info': stripe.Customer.retrieve(organization.stripe_token)})
        else:
            return Response({'message': 'Organization has no credit cards'},
                            status=status.HTTP_404_NOT_FOUND)


@swagger_auto_schema(
    method='POST',
    operation_description="Update the organization plan and update the counts",
    request_body=None,
    responses={202: MessageResponse},
)
@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def update_premium_configs(request, organization_id):
    try:
        request_body = request.body
        json_body = json.loads(request_body)
        seat_count = json_body['seat_count']
        type = json_body['type']
        user = request.user

        organization = Organization.objects.get(id=organization_id)
    except Organization.DoesNotExist:
        return Response({'message': 'Organization does not exists'}, status=status.HTTP_404_NOT_FOUND)
    else:
        try:
            if not organization.stripe_token:
                return Response({'message': 'No credit cards associated with this organization'},
                                status=status.HTTP_406_NOT_ACCEPTABLE)
            elif organization.subscription_id:
                stripe.Subscription.modify(
                    organization.subscription_id,
                    cancel_at_period_end=False,
                    quantity=seat_count
                )

                organization.seat_count = seat_count
                organization.cancel_at_period_end = False
                organization.save()
            else:
                subscription = stripe.Subscription.create(
                    customer=organization.stripe_token,
                    quantity=seat_count,
                    plan=settings.STRIPE_PLAN_ID,
                    default_tax_rates=[settings.STRIPE_TAX_KEY]
                )

                organization.subscription_id = subscription.id
                organization.paid_until = datetime.fromtimestamp(
                    subscription.current_period_end)
                organization.seat_count = seat_count
                organization.recipe_count = -1
                organization.type = type
                organization.save()
        except stripe.error.StripeError as e:
            body = e.json_body
            err = body.get('error', {})
            return Response(err, status=e.http_status)
        except Exception as e:
            return Response({'message': e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            compose_change_plan_message(user, organization)
            return Response({
                'message': 'Updated the organization plan',
                'organization': OrganizationSerializer(organization).data},
                status=status.HTTP_202_ACCEPTED
            )


@swagger_auto_schema(
    method='POST',
    operation_description="Update the organization plan to while canceling any active subscription",
    request_body=None,
    responses={202: MessageResponse},
)
@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def update_free_configs(request, organization_id):
    try:
        organization = Organization.objects.get(id=organization_id)
    except Organization.DoesNotExist:
        return Response({'message': 'Organization does not exists'}, status=status.HTTP_404_NOT_FOUND)
    else:
        try:
            if organization.subscription_id:
                stripe.Subscription.modify(
                    organization.subscription_id,
                    cancel_at_period_end=True
                )

            organization.cancel_at_period_end = True
            organization.save()
        except stripe.error.StripeError as e:
            body = e.json_body
            err = body.get('error', {})
            return Response(err, status=e.http_status)
        except Exception as e:
            return Response({'message': e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response({
                'message': 'Updated the organization plan',
                'organization': OrganizationSerializer(organization).data},
                status=status.HTTP_202_ACCEPTED
            )


@api_view(['POST'])
@permission_classes((IsAuthenticated, ImBillingOwner,))
def cancel_plan(request):
    organization = request.organization
    
    if not organization.stripe_token:
        return Response({'message': 'No credit cards associated with this organization'},
            status=status.HTTP_406_NOT_ACCEPTABLE)
    elif organization.subscription_id:
        subscription = stripe.Subscription.retrieve(organization.subscription_id)
        subscription_type = subscription.plan.interval
        
        time_since_plan_start = time.time() - subscription.current_period_start
        
        try:
            if subscription_type == 'year' and time_since_plan_start <= ANNUAL_PLAN_REFUND_PERIOD and subscription.status != 'trialing':
                return Response({'message': 'Cancel failed: you are eligiable for a refund'}, status=status.HTTP_406_NOT_ACCEPTABLE)
            elif subscription.cancel_at_period_end:
                return Response({'message': 'You have already cancelled your susbscription'}, status=status.HTTP_406_NOT_ACCEPTABLE)
            else:
                stripe.Subscription.modify(
                    organization.subscription_id,
                    cancel_at_period_end=True
                )
                organization.cancel_at_period_end = True
                organization.save()
            
            return Response({'message': 'Cancelled subscription', 'current_period_end': subscription.current_period_end}, status=status.HTTP_200_OK)
        
        except stripe.error.StripeError as e:
            body = e.json_body
            err = body.get('error', {})
            return Response(err, status=e.http_status)
        except Exception as e:
            return Response({'message': e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            
            
@api_view(['POST'])
@permission_classes((IsAuthenticated, ImBillingOwner,))   
def downgrade_plan_and_refund(request):
    organization = request.organization
    
    if not organization.stripe_token:
        return Response({'message': 'No credit cards associated with this organization'},
            status=status.HTTP_406_NOT_ACCEPTABLE)
    
    elif organization.subscription_id:
        subscription = stripe.Subscription.retrieve(organization.subscription_id)
        subscription_type = subscription.plan.interval
        
        user_organization = UserOrganization.objects.filter(organization=request.organization)
        user_count = user_organization.count()

        time_since_plan_start = time.time() - subscription.current_period_start
        
        try:
            if(subscription_type == 'year' and 
            time_since_plan_start < ANNUAL_PLAN_REFUND_PERIOD and 
            user_count <= 2):
                # issue refund
                latest_invoice = stripe.Invoice.retrieve(subscription.latest_invoice)
                charge_id = latest_invoice.charge
                stripe.Refund.create(charge=charge_id)
                
                # change subscription to 2 seats
                stripe.Subscription.modify(
                    organization.subscription_id,
                    quantity=2
                )

                return Response({'message': 'Refund issued successfully'}, status=status.HTTP_200_OK)
            else:
                return Response({'message': 'You are ineligiable for a refund'}, status=status.HTTP_406_NOT_ACCEPTABLE)
        except stripe.error.StripeError as e:
            body = e.json_body
            err = body.get('error', {})
            return Response(err, status=e.http_status)
        except Exception as e:
            return Response({'message': e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['POST'])
@permission_classes((IsAuthenticated, ImBillingOwner,))   
def undo_cancellation(request):
    organization = request.organization

    if not organization.stripe_token:
        return Response({'message': 'No credit cards associated with this organization'},
            status=status.HTTP_406_NOT_ACCEPTABLE)
        
    elif organization.subscription_id:
        subscription = stripe.Subscription.retrieve(organization.subscription_id)
            
        try:
            if (
                subscription.cancel_at_period_end and 
                subscription.quantity > 2 and
                (subscription.status == 'active' or subscription.status == 'trialing')
            ):

                stripe.Subscription.modify(
                    organization.subscription_id,
                    cancel_at_period_end=False
                )
                organization.cancel_at_period_end = False
                organization.save()

                return Response({'message': 'Successfully undone cancellation'}, status=status.HTTP_200_OK)
            else:
                return Response({'message': 'Undo failed'}, status=status.HTTP_406_NOT_ACCEPTABLE)
        except stripe.error.StripeError as e:
            body = e.json_body
            err = body.get('error', {})
            return Response(err, status=e.http_status)
        except Exception as e:
            return Response({'message': e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
@permission_classes((IsAuthenticated, ImAdminOrManager,))
def user_search(request):
    organization = request.organization

    search_text = request.query_params.get('text', '')
    pre_users = User.objects.filter(
        Q(first_name__iexact=search_text) |
        Q(last_name__iexact=search_text)
    ).distinct()

    if pre_users.exists():
        user_organization = UserOrganization.objects.filter(user__in=pre_users, organization=organization, is_active=True)
        if user_organization.exists():
            users = user_organization.values_list('user', flat=True)
            post_users = User.objects.filter(id__in=users)
            return Response(UserForSearch(post_users, many=True).data, status=status.HTTP_200_OK)

    return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def user_search_es(request):
    connections.create_connection(hosts=[ELASTICSEARCH_HOST])
    search_text = request.query_params.get('text', '')
    schema_name = request.META.get('HTTP_X_DTS_SCHEMA')
    organization = Organization.objects.get(schema_name=schema_name)
    index = f"users-{organization.schema_name}"

    # Elasticsearch
    q = {
        "query": {
            "multi_match": {
                "query": search_text,
                "fields": ["first_name", "last_name"],
            }
        }
    }
    search = Search(index=index)
    search.update_from_dict(q)
    results = search.execute()

    users = User.objects.filter(id__in=[h.id for h in results])
    return Response(SingleUserSerializer(users, many=True).data)


@api_view(['POST'])
@permission_classes((AllowAny,))
def stripe_webhook(request):
    payload = request.body
    sig_header = request.META['HTTP_STRIPE_SIGNATURE']
    event = None

    try:
        event = stripe.Webhook.construct_event(
            payload, sig_header, settings.STRIPE_SECRET_WEBHOOK
        )
    except ValueError as e:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    except stripe.error.SignatureVerificationError as e:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    else:
        try:
            if event.type.startswith('invoice.'):
                handle_invoice_event(event)
            elif event.type.startswith('customer.'):
                handle_customer_event(event)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            '''
            NOTE: If for example we manually do some work on the stripe dashboard that is
            not associated to a organization, we should just aknowledge that we got the
            webhook, but dont fail it
            '''
            return Response({"message": "There was an error when trying to fetch info for the webhook event", "data": str(e)}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"message": "An exception happened while processing the event webhook", "data": e}, status=status.HTTP_406_NOT_ACCEPTABLE)
        else:
            return Response(status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def get_organization_by_domain(request):
    domain = request.query_params.get('domain', None)

    try:
        organization_query = Organization.objects.filter(Q(domain_url__iexact=domain) | Q(old_domains__contains=[domain]))
        if organization_query.count() == 0:		
            return Response({'message': 'Organization does not exists'}, status=status.HTTP_404_NOT_FOUND)
        else:
            return Response({
                'message': 'Found organization',
                'organization': PublicOrganizationSerializer(organization_query[0]).data},
                status=status.HTTP_200_OK
            )
    except Exception:
        return Response({'message': 'Organization does not exists'}, status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_user_from_token(request):
    user = request.user

    try:
        current_user = request.user
        user_organizations = UserOrganization.objects.filter(user=current_user, is_active=True).values('organization')
        organizations = Organization.objects.filter(id__in=user_organizations)
    except Exception as e:
        return Response()

    return Response({'user': UserSerializer(user).data, 'organizations': OrganizationSerializer(organizations, many=True).data}, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_inactive_organizations(request):
    user = request.user

    try:
        current_user = request.user
        user_organizations = UserOrganization.objects.filter(user=current_user, is_active=False).values('organization')
        organizations = Organization.objects.filter(id__in=user_organizations)
    except Exception as e:
        return Response()

    return Response({'organizations': OrganizationSerializer(organizations, many=True).data}, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated, ImAdmin,))
def get_invoices(request):
    try:
        organization = request.organization
    except Organization.DoesNotExist:
        # TODO: remove this exception
        return Response({'message': 'Organization or schema name does not exists'}, status=status.HTTP_404_NOT_FOUND)
    else:
        if not organization.stripe_token:
            return Response({'message': 'Organization doesnt have a stripe account'}, status=status.HTTP_406_NOT_ACCEPTABLE)

        try:
            incoming_invoice = stripe.Invoice.list(customer=organization.stripe_token)
        except stripe.error.InvalidRequestError as e:
            return Response({'message': 'Organization doesnt have a subscription'}, status=status.HTTP_409_CONFLICT)
        return Response(incoming_invoice, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((AllowAny,))
def request_invitation(request):
    raw_data = request.body
    data = json.loads(raw_data)

    try:
        organization = Organization.objects.get(id=data['org_id'])
        user = User.objects.get(email=data['email'].lower())
        user_organization = UserOrganization.objects.get(user=user, organization=organization)
    except ValueError as e:
        return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)
    except Organization.DoesNotExist:
        return Response({'message': 'Organization does not exists.'}, status=status.HTTP_404_NOT_FOUND)
    except (User.DoesNotExist, UserOrganization.DoesNotExist):
        serialized = RequestInvitationSerializer(data=data)

        if serialized.is_valid():
            admins = UserOrganization.objects.filter(organization=organization, permission='admin', is_active=True)
            for admin in admins:
                user = admin.user
                send_request_invitation_message(
                    serialized.data['first_name'], serialized.data['last_name'], serialized.data['email'], organization, user.first_name, user.email)
            return Response(
                {'message': f"Thanks! We've contacted the administrator to request permission for you to join the organization {organization.name}. If you're approved, you'll see an e-mail from us in your inbox!"},
                status=status.HTTP_202_ACCEPTED
            )
        else:
            return Response({'message': 'Body data is not valid.'}, status=status.HTTP_406_NOT_ACCEPTABLE)
    else:
        if not user.first_name:
            token, created = Token.objects.get_or_create(user=user, used=False)
            send_invitation_message(user, organization, token)
        else:
            send_organization_login(user, organization)

        return Response(
            {'message': f"Thanks! Please check your e-mail to log in to {organization.name} on KnowHow"},
            status=status.HTTP_202_ACCEPTED
        )


@api_view(['PATCH'])
@permission_classes((AllowAny,))
def update_role_with_token(request):
    try:
        request_body = request.body.decode()
        json_body = json.loads(request_body)
        email = json_body['email']
        token = json_body['token']
        role = json_body['role']
        organization = json_body['organization']

        user = User.objects.get(email=email)
        organization = Organization.objects.get(id=organization)
        token = Token.objects.get(user=user, token=token, used=False)
        user_organization = UserOrganization.objects.get(user=user, organization=organization)
    except User.DoesNotExist:
        return Response({'message': 'User doesnt exist'}, status=status.HTTP_406_NOT_ACCEPTABLE)
    except Token.DoesNotExist:
        return Response({'message': 'Token doesnt exist'}, status=status.HTTP_406_NOT_ACCEPTABLE)
    except KeyError:
        return Response({'message': 'Given fields are not valid.'}, status=status.HTTP_406_NOT_ACCEPTABLE)
    else:
        token.used = True
        token.save()
        user_organization.title = role
        user_organization.save()

        return Response({'message': 'Member updated'}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((ImAdmin,))
def add_admin_to_howto_org(request):
    try:
        user = request.user
        organization = request.organization
        UserOrganization.objects.get(user=user, organization=organization, permission='admin')
        howto_org = Organization.objects.get(schema_name='how_to')
    except UserOrganization.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    except Organization.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    else:
        user_organization, _ = UserOrganization.objects.get_or_create(user=user, organization=howto_org)
        user_organization.permission = 'membe'
        user_organization.save()
        serializer = OrganizationSerializer(user_organization.organization)
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)


@api_view(['POST'])
def request_to_upgrade(request):
    try:
        admin_id = request.data.get('admin', None)
        message = request.data.get('message', None)

        user_organization = UserOrganization.objects.get(user_id=admin_id, organization=request.organization,
                                                         permission='admin')
    except UserOrganization.DoesNotExist:
        return Response({'message': 'Given admin ID does not exists or it\'s not an admin.'},
                        status=status.HTTP_404_NOT_FOUND)
    else:
        send_message_admin_to_upgrade(user_organization.user, request.user, message, user_organization.organization)
        return Response(status=status.HTTP_202_ACCEPTED)


class SingleUser(generics.RetrieveAPIView):
    queryset = User.objects.filter(is_active=True)
    serializer_class = SingleUserSerializer
    permission_classes = (IsAuthenticated, WeArePartners, ImAdminOrManager | MemberCanSeeMember)


class CreateUser(generics.CreateAPIView):
    """
    Create a new user
    """
    serializer_class = UserSerializer
    permission_classes = (AllowAny,)


class UpdateUser(generics.UpdateAPIView):
    """
    Update a user. only for users who have never logged in.
    """
    queryset = User.objects.all()
    lookup_field = 'email'
    serializer_class = UserSerializer
    permission_classes = (AllowAny,)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.last_login is None:
            return super().update(request, *args, **kwargs)
        else:
            raise ValidationError(
                {'message': 'This user can not be updated using this endpoint.'})


class BaseUpdateUser(generics.UpdateAPIView):
    """
    Update user.
    """
    queryset = User.objects.all()
    serializer_class = BaseUserSerializer
    permission_classes = (IsAuthenticated,)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            title = request.data['title']
            user_organization = UserOrganization.objects.get(user=instance, organization=request.organization)
        except KeyError:
            pass
        except AttributeError:
            pass
        except UserOrganization.DoesNotExist:
            if request.user.is_superuser:
                pass
            else:
                return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
        else:
            user_organization.title = title
            user_organization.save()

        return super().update(request, *args, **kwargs)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


class CreateOrganization(generics.CreateAPIView):
    """
    Create a new Organization
    """
    serializer_class = OrganizationSerializer
    permission_classes = (AllowAny,)

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=False)
        organization_name = serializer.data.get('name')
        compose_created_organization_message(organization_name, request.user)
        return response


class GetOrganization(generics.RetrieveAPIView):
    """
    Create a new Organization
    """
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer
    permission_classes = (IsAuthenticated,)


class UpdateOrganization(generics.UpdateAPIView):
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer
    permission_classes = (IsAuthenticated,)


class UserPermissionViewSetClone(viewsets.ModelViewSet):
    queryset = UserOrganization.objects.all()
    lookup_field = 'user'

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if self.action == 'create':
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]

    def get_queryset(self):
        request = self.request
        return UserOrganization.objects.filter(organization=request.organization)

    def get_serializer_class(self):
        if self.action == 'list':
            return UserPermissionDetailedCloneSerializer
        return UserPermissionCloneSerializer

    @action(detail=False, methods=['GET'])
    def managers_and_admins(self, request):
        users = self.get_queryset().filter(Q(permission='admin') | Q(permission='manag'))

        serializer = self.get_serializer(users, many=True)
        return Response(serializer.data)

    @action(detail=False, methods=['GET'])
    def get_admins(self, request):
        users = self.get_queryset().filter(permission='admin', is_active=True)

        serializer = self.get_serializer(users, many=True)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        user = request.user
        if user.is_superuser:
            temp_user_organization = UserOrganization(user=user, organization=request.organization, permission='admin')
            serializer = self.get_serializer(temp_user_organization)
            return Response(serializer.data)
        else:
            return super().retrieve(request, *args, **kwargs)


class ChangePasswordView(generics.UpdateAPIView):
    """
    Endpoint for changing password.
    """
    serializer_class = ChangePasswordSerializer
    model = User
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({'message': 'Given old password is wrong.'}, status=status.HTTP_400_BAD_REQUEST)

            new_password_1 = serializer.data.get("new_password1")
            new_password_2 = serializer.data.get("new_password2")
            if new_password_1 != new_password_2:
                return Response({'message': 'New passwords must be the same.'}, status=status.HTTP_400_BAD_REQUEST)

            # set_password also hashes the password that the user will get
            self.object.set_password(new_password_1)
            self.object.save()
            return Response({'message': 'Password changed.'}, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
